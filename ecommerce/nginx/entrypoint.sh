#!/usr/bin/env sh

mkdir /run/php/
touch /run/php/php7.3-fpm.sock
php-fpm7.3
# Run nginx
nginx -g "daemon off;"