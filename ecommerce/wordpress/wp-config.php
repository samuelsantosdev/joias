<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'wordpress' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'mysql' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'mysql' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ' P)EiAP5$w6z>fh%u^eWO])s|>pZhg>2]r,8J7bH-0_ch09y3KQ-d&$N`e4a<#~y' );
define( 'SECURE_AUTH_KEY',  '.|b)+Bx?y/hwUJ^eQV+WSivuk{?JJ0VF?+&OJ|0Zp/0W;8Z=>Wd4|?oT%gWSmJ~;' );
define( 'LOGGED_IN_KEY',    'q>NP|%QT:xGC7kRZ#C[yl~^wtO)6^$ T[%I<~x]0jZ)c(K!B;MlIXmBC9,7#5*e&' );
define( 'NONCE_KEY',        '!c5KQ+o8 *ma^4wv+3O#3Ny) w4U^93I.R=k u8N6&kkkkt=K2A5VEoMS,f]lu,X' );
define( 'AUTH_SALT',        'eufm[{11Ljq6!aWtTr4(W.<^%#Mk-H^e{}n/L>FxT`+uCKT+C*O7W(opF1bP#iMF' );
define( 'SECURE_AUTH_SALT', 'R7:J1=J~*8Rb4uTLDB7kRJr2)1m,4;@<qXD(*VxbTU-k9-.i`HB~lX(8}=#Dt>H6' );
define( 'LOGGED_IN_SALT',   '@PfA t}OY%</IdRL1mKJq;?Fp-H+I%l[Y-.e6 GI49fmUDp#XvbrVtHrJgTHdg>r' );
define( 'NONCE_SALT',       'gr5[Hdt;3YI9+/U{~:XL~p*{{2HlA<K}uJpV)*:WfnS0O,&9.H,v$8uVL$$x`K4o' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
define('FS_METHOD', 'direct');

require_once(ABSPATH . 'wp-settings.php');

