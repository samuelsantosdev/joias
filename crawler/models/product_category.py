""" Model to user table """
from peewee import *
import logging
import datetime
from models.database import db
from models.base_schemas import BaseSchema
from models.product import Product
from models.category import Category

class ProductCategory(Model):

    product         = ForeignKeyField(Product, backref='products')
    category        = ForeignKeyField(Category, backref='categories')
    status          = IntegerField(default=1)
    date_insert     = DateTimeField(default=datetime.datetime.now)

    class Meta:
        indexes = (
            # Unique
            (('product', 'category'), True),
        )
        database = db

    class Schema(BaseSchema):

        fields = ['name', 'email']
