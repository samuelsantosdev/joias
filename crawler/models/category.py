""" Model to user table """
from peewee import *
import logging
import datetime
from models.database import db
from models.base_schemas import BaseSchema

class Category(Model):

    name            = CharField(unique=True)
    status          = IntegerField(default=1)
    date_insert     = DateTimeField(default=datetime.datetime.now)
    
    class Meta:
        indexes = (
            (('status'), True)
        )
        database = db

    class Schema(BaseSchema):

        fields = ['name']
