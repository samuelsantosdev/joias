import logging

class BaseSchema():

    def __init__(self, model):
            self.model = model

    def _make_list(self):
        list_return=[]
        dict_return={}
        for model in self.model:
            for field in self.fields:
                dict_return[field] = model.__dict__['__data__'][field]

            list_return.append(dict_return) 
            dict_return={}

        logging.warning(list_return)
        return list_return

    def _make_dict(self):
        dict_return={}
        logging.warning(self.model.__dict__['__data__'])
        for field in self.fields:
            dict_return[field] = self.model.__dict__['__data__'][field]
        return dict_return


    def get(self, many:bool=False):

        if many:
            return self._make_list()
        else:
            return self._make_dict()