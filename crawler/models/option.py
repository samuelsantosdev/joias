""" Model to user table """
from peewee import *
import logging
import datetime
from models.database import db
from models.base_schemas import BaseSchema

class Option(Model):

    name            = CharField(unique=True)
    status          = IntegerField(default=1)
    date_insert     = DateTimeField(default=datetime.datetime.now)

    class Meta:
        database = db

    class Schema(BaseSchema):

        fields = ['name']
