""" Model to user table """
from peewee import *
import logging
import datetime
from models.database import db
from models.base_schemas import BaseSchema
from models.product import Product
from models.option import Option

class ProductOption(Model):

    product         = ForeignKeyField(Product, backref='products')
    option          = ForeignKeyField(Option, backref='options')
    status          = IntegerField(default=1)
    date_insert     = DateTimeField(default=datetime.datetime.now)
    value           = CharField()

    class Meta:
        indexes = (
            # Unique
            (('product', 'option', 'value'), True),
        )
        database = db

    class Schema(BaseSchema):

        fields = ['name', 'email']
