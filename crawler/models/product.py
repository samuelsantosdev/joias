""" Model to user table """
from peewee import *
import logging
import datetime
from models.database import db
from models.base_schemas import BaseSchema

class Product(Model):

    name            = CharField(unique=True)
    external_code   = CharField(unique=True)
    internal_code   = CharField()
    description     = TextField(default=None)
    amount_one      = DecimalField()
    amount_mult     = DecimalField()
    status          = IntegerField(default=1)
    date_insert     = DateTimeField(default=datetime.datetime.now)

    class Meta:
        database = db

    class Schema(BaseSchema):

        fields = ['name', 'internal_code', 'ammount_one', 'ammount_mult']
