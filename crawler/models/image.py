""" Model to user table """
from peewee import *
import logging
import datetime
from models.database import db
from models.base_schemas import BaseSchema
from models.product import Product

class Image(Model):

    path            = CharField(unique=True)
    status          = IntegerField(default=1)
    date_insert     = DateTimeField(default=datetime.datetime.now)
    product         = ForeignKeyField(Product, backref='images')

    class Meta:
        indexes = (
            # Unique
            (('path', 'product'), True),
        )
        database = db

    class Schema(BaseSchema):

        fields = ['path']
