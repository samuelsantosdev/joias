from peewee import MySQLDatabase
import settings

# Connect to a MySQL database on network.
db = MySQLDatabase(settings.NAME_DATABASE, user=settings.USER_DATABASE, password=settings.PASSWORD_DATABASE,
                         host=settings.HOST_DATABASE, port=settings.PORT_DATABASE)
