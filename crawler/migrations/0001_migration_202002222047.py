# auto-generated snapshot
from peewee import *
import datetime
import peewee


snapshot = Snapshot()


@snapshot.append
class Product(peewee.Model):
    name = CharField(max_length=255, unique=True)
    external_code = CharField(max_length=255, unique=True)
    internal_code = CharField(max_length=255)
    description = TextField()
    ammount_one = DecimalField(auto_round=False, decimal_places=5, max_digits=10, rounding='ROUND_HALF_EVEN')
    ammount_mult = DecimalField(auto_round=False, decimal_places=5, max_digits=10, rounding='ROUND_HALF_EVEN')
    status = IntegerField(default=1)
    date_insert = DateTimeField(default=datetime.datetime.now)
    class Meta:
        table_name = "product"


@snapshot.append
class Image(peewee.Model):
    path = CharField(max_length=255, unique=True)
    status = IntegerField(default=1)
    date_insert = DateTimeField(default=datetime.datetime.now)
    product = snapshot.ForeignKeyField(backref='images', index=True, model='product')
    class Meta:
        table_name = "image"


