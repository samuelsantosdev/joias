import scrapy
import os
import settings
import logging
import html2text
import re
import unidecode
import uuid
import traceback
import json


from models.option import Option
from models.product_option import ProductOption
from models.product_category import ProductCategory
from models.category import Category
from models.product import Product
from models.image import Image
from woocommerce import API

def debug_msg(msg:str):
    logging.debug("*"*10)
    logging.debug(msg)
    logging.debug("*"*10)

def to_decimal(amount:str):
    return re.sub(r'[a-b]+', '', amount.lower().replace('r$', '').strip().replace('.','').replace(',','.'))
        
def clean_str(s:str):
    return re.sub(' +', ' ', s)
class Crawler(scrapy.Spider):

    name = settings.NAME_PAGE
    start_urls = [settings.URL_PAGE]

    def __init__(self):
        super().__init__()
        self._list_uris         = []
        self._list_categories   = []
        self._product_page      = []

    def parse(self, response):

        self.sync()
        return

        links = response.css('#navegacao_fundo .box_navegacao ul li.menu-item a').xpath('@href').getall()
        categories = response.css('#navegacao_fundo .box_navegacao ul li.menu-item a::text').getall()

        self._list_uris         = [ link.replace(self.start_urls[0], '') for link in links if link != self.start_urls[0] ]
        self._list_categories   = [ category for category in categories if category != '' ]

        logging.info(self._list_uris)
        logging.info(self._list_categories)

        self.save_main(self._list_categories)

        for num, pge_product in enumerate(self._list_uris):
            req = scrapy.Request( '{}{}'.format(self.start_urls[0],self._list_uris[num]), callback=self.parse_products_page)
            req.meta['category'] = self._list_categories[num]
            yield req

        self.sync()
        return

    def parse_products_page(self, response):

        products_list_url = response.css('.vitrine .box-produtos a.foto-produto').xpath('@href').getall()
        list_url_page = response.css('#bot-conteudo ul.paginacao li a').xpath('@href').getall()
        list_url_page.insert(0, '?pg=1&')
        debug_msg(list_url_page)
        for num, page_url in enumerate(products_list_url):
            for page in list_url_page:
                req = scrapy.Request("{}{}".format(page_url, page), callback=self.parse_product)
                req.meta['category'] = response.meta.get('category')
                yield req
            
    def parse_product(self, response):
            
        """ Color material """
        h = html2text.HTML2Text()

        properties = response.css('#conteudo .box-detalhe .box-propriedades table tr td').getall()
        properties_parsed = [ re.sub(r'[^0-9a-zA-Z\,\- \s\n\/]+', '', 
                        h.handle( unidecode.unidecode(
                            prop.replace('</option>', '<br></option><br>\n')
                        ) )
                    ).strip().split("\n") for prop in properties ]

        desc = h.handle(response.selector.xpath("//div[@class='descricao']").get())

        image = response.css('#conteudo .box-detalhe #galeria img').xpath('@src').get()
        
        """  one multiple """
        prices = response.css('#conteudo .box-detalhe .preco::text').getall()
        product_amount_multiple = to_decimal(prices[0]) if len(prices) > 0 else 0
        product_amount_one = to_decimal(prices[1]) if len(prices) > 1 else 0

        data = {
            'product_code' : response.css('#conteudo .box-detalhe .produto_detalhes::text').get(),
            'product_description' : desc,
            'product_amount_multiple' : product_amount_multiple,
            'product_amount_one' : product_amount_one,
            'options' : properties_parsed,
            'photo' : image,
            'category' : response.meta.get('category')
            }

        self.save_prod(data)

        yield data
    
    def save_prod(self, data):

        options_to_link=[]

        for option in data['options']:

            option_name = option[0]

            if Option.select().where(Option.name == option_name).count() == 0:
                new_opt = Option(name=option_name)
                new_opt.save()
                
                logging.info("Option {} add".format(option_name))
            else:
                logging.info("Option {} exists".format(option_name))
        
            options_to_link.append({
                "obj" : Option.get(Option.name == option_name),
                "values" : list(filter(lambda a: a.strip() != '', option[1:]))
                })

        
        try:

            if Product.select().where(Product.external_code == data['product_code']).count() == 0:
    
                code = str(uuid.uuid4())
                prod = Product(name=data['product_code'],
                        external_code=data['product_code'], 
                        description=data['product_description'],
                        internal_code=code,
                        amount_mult=data['product_amount_multiple'], 
                        amount_one=data['product_amount_one'])
                prod.save()
            else: 
                prod = Product.get(Product.external_code == data['product_code'])

            cat = Category.get(Category.name == data['category'])
            
            if Image.select().where(Image.path == data['photo']).count() == 0:
                img = Image(path=data['photo'], product=prod)
                img.save()
            else:
                img = Image.get(Image.path == data['photo'])

            logging.info("Image {} add".format(img.path))

            if ProductCategory.select().where( (ProductCategory.category == cat) & (ProductCategory.product == prod) ).count() == 0:
                pc = ProductCategory(category=cat, product=prod)
                pc.save()
                logging.info("ProdCat {} add".format(cat.name))

            for opt in options_to_link:
                for val in opt['values']:

                    if ProductOption.select().where( (ProductOption.option == opt['obj']) & \
                        (ProductOption.product == prod) & \
                        (ProductOption.value == val) ).count() == 0:
                        po = ProductOption(option=opt['obj'], product=prod, value=val)
                        po.save()
                        logging.info("ProdOpt {} add".format(opt['obj'].name))

            logging.info("Prod {} add".format(prod.name))
        
        except Exception as error:

            track = traceback.format_exc()
            logging.info("Error add prod - {}".format(track))
                
            
        

    def save_main(self, data):
    
        for category in data:

            cat = Category.select().where(Category.name == category).count()
            if cat == 0:
                new_cat = Category(name=category)
                new_cat.save()
                logging.info("Category {} add".format(category))
            else:
                logging.info("Category {} exists".format(category))

    def sync(self):

        wcapi = API(
                url=settings.WC_URL,
                consumer_key=settings.WC_CONSUMER_KEY,
                consumer_secret=settings.WC_CONSUMER_SECRET,
                version=settings.WC_VERSION
            )

        categories = Category.select(Category.name).dicts().execute()
        
        [ wcapi.post("products/categories", {"name":name['name']}) for name in categories ]
        categories_wc = wcapi.get("products/categories?per_page=100&").json()

        options = Option.select(Option.name).dicts().execute()

        [ wcapi.post("products/attributes", {"name":name['name']}) for name in options ]

        attributes_wc = wcapi.get("products/attributes").json()

        products = Product.select().dicts().execute()

        for prod in products:

            cat = Category.get(Category.id == ProductCategory.get(ProductCategory.product == prod['id']).category_id).name

            cat_id = list(filter( lambda x: clean_str(x['name']).strip() == clean_str(cat).strip(), categories_wc))[0]['id']
            
            options = ProductOption.select().where(ProductOption.product == prod['id']).dicts().execute()
            opt_id = []
            opts_agg = []
            valor = prod['amount_one']

            variations = {"stock_status" : 'instock',
                    "regular_price": str(valor * 2),'attributes':[]}
            

            for op in options:
                opt = Option.get(Option.id == op['option']).name
                o = list(filter( lambda x: clean_str(x['name']).strip() == clean_str(opt).strip(), attributes_wc))[0]
                list_actual = list(filter( lambda c: c['id'] == o['id'], opts_agg ))

                variations['attributes'].append({
                    "id": o['id'],
                    "option": op['value']
                })

                if len(list_actual) == 0:
                    opts_agg.append({
                        "id": o['id'], 
                        "name":o['name'],
                        "variation":True,
                        "visible":True, 
                        "options": [op['value']]
                        })
                else: 
                    opts_agg[opts_agg.index(list_actual[0])]['options'].append(op['value'])


            image = Image.get(Image.product == prod['id']).path

            data = {
                "name": cat,
                "sku" : prod['name'],
                "type": "variable",
                "stock_status" : 'instock',
                "regular_price": str(valor * 2),
                "sale_price": str(valor * 2),
                "description": prod['description'],
                "virtual" : False,
                "weight": "20",
                "dimensions" : {
                    "length":"10",
                    "width":"10",
                    "height":"10"
                },
                "short_description": " ".join(prod['description'].split(' ')[0:8]) + '...',
                "categories": [
                    {
                        "id":  cat_id
                    }
                ],
                "attributes":opts_agg,
                "images": [
                    {
                        "src": image
                    }
                ]
            }

            try:
                debug_msg(data)
                cad = wcapi.post("products", data).json()
                
                for op in options:
                    opt = Option.get(Option.id == op['option']).name
                    o = list(filter( lambda x: clean_str(x['name']).strip() == clean_str(opt).strip(), attributes_wc))[0]

                    variations = {"stock_status" : 'instock',
                    "regular_price": str(valor * 2),'attributes':[{
                        "id": o['id'],
                        "option": op['value']
                    }]}

                    aaa = wcapi.post("products/{}/variations".format(cad['id']), variations).json()
                    debug_msg(aaa)
                    
                
            except Exception as error:
                logging.error(str(error))
