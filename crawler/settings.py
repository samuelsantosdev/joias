import os
import logging
from dotenv import load_dotenv
load_dotenv()

try:

    NAME_PAGE=os.environ['NAME_PAGE']
    URL_PAGE=os.environ['URL_PAGE']
    NAME_DATABASE=os.environ['NAME_DATABASE']
    USER_DATABASE=os.environ['USER_DATABASE']
    PASSWORD_DATABASE=os.environ['PASSWORD_DATABASE']
    HOST_DATABASE=os.environ['HOST_DATABASE']
    PORT_DATABASE=int(os.environ['PORT_DATABASE'])
    PASSWORD_ADMIN=os.environ['PASSWORD_ADMIN']

    enviroment = str(os.environ['ENVIRONMENT']).upper()
    
    WC_URL=os.environ['WC_URL_{}'.format(enviroment)]
    WC_CONSUMER_KEY=os.environ['WC_CONSUMER_KEY_{}'.format(enviroment)]
    WC_CONSUMER_SECRET=os.environ['WC_CONSUMER_SECRET_{}'.format(enviroment)]
    WC_VERSION=os.environ['WC_VERSION_{}'.format(enviroment)]

except KeyError as error:
    logging.error(str(error))