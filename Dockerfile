FROM python:3.6-alpine3.7
RUN addgroup -g 1042 -S gcs && \
    adduser -u 1042 -S gcs -G gcs

ENV HOME_PROJECT="/var/task"

RUN pip3 install --upgrade pip
RUN apk add --no-cache --virtual .build-deps gcc musl-dev python3-dev libressl-dev libffi-dev libxml2-dev libxslt-dev \  
 make libffi-dev bash zip

RUN mkdir "$HOME_PROJECT"
COPY ./crawler/requirements.txt "$HOME_PROJECT"
COPY ./crawler/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
WORKDIR "$HOME_PROJECT"

RUN pip3 install -r requirements.txt

EXPOSE 80
ENTRYPOINT [ "/entrypoint.sh" ]